<?php

namespace Drupal\preloader;

use DarkGhostHunter\Preloader\Opcache;
use DarkGhostHunter\Preloader\Preloader;
use DarkGhostHunter\Preloader\PreloaderLister;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\Finder\Finder;

/**
 * PreloaderGenerator service.
 */
class PreloaderGenerator {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * @var \DarkGhostHunter\Preloader\Preloader
   */
  protected $preloader;

  /**
   * Constructs a PreloaderGenerator object.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension_list
   *   Module extension list service.
   */
  public function __construct(FileSystemInterface $file_system, ModuleExtensionList $module_extension_list) {
    $this->fileSystem = $file_system;
    $this->setupPreloader($module_extension_list);
  }

  /**
   * Setup the preloader generation script.
   *
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension_list
   *   Module extension list service.
   */
  protected function setupPreloader(ModuleExtensionList $module_extension_list): void {
    $this->preloader = new Preloader(
      new DrupalPreloaderCompiler($module_extension_list),
      new PreloaderLister(),
      new Opcache()
    );
    $this->preloader->selfExclude();
    $this->preloader->ignoreNotFound();
    $this->setMemoryLimit();
  }

  /**
   * Set the memory limit for scripts that should be preloaded.
   *
   * @param int $memory_limit
   *   Memory limit in MiB.
   */
  public function setMemoryLimit(int $memory_limit = 128) {
    $this->preloader->memoryLimit(128);
  }

  public function writePreloaderScript(string $filename, array $excluded_directories = []) {
    $exclude_list = array_map(
      fn ($dir) => $this->fileSystem->realpath(DRUPAL_ROOT . '/' . $dir),
      $excluded_directories
    );

    // Exclude files we don't want in the preloader. Apart from our exclude
    // list, we should skip files that may cause side-effects.
    // See shouldSkipFile() method for more details.
    $this->preloader->exclude(function (Finder $finder) use ($exclude_list) {
      $finder
        // We are assuming that the vendor directory is one level above Drupal.
        // @TODO: Find a better way.
        ->in(DRUPAL_ROOT . '/..')
        ->filter(fn (\SplFileInfo $file) => $this->shouldSkipFile($file, $exclude_list));
    });
    $this->preloader->useRequire(DRUPAL_ROOT . '/autoload.php');

    $target_filename = $this->fileSystem->realpath($filename);
    $this->preloader->writeTo($target_filename);
  }

  /**
   * Generate a default target file name with random characters.
   *
   * @return string
   *   Filename
   */
  public function getDefaultTargetFilename(): string {
    $random = '';
    for ($i = 0; $i < 10; $i++) {
      $random .= chr(mt_rand(65, 90));
    }
    return 'public://php/preloader/preloader.' . $random . '.php';
  }

  /**
   * Check if a file is writable.
   *
   * @param string $filename The filename to test if it is writable.
   *
   * @return bool
   *   TRUE if the file is writable.
   */
  public function isWritable(string $filename): bool {
    // Create the directory first.
    $dir = dirname($filename);
    if (!$this->fileSystem->prepareDirectory($dir, 3)) {
      return FALSE;
    }

    if (!touch($filename)) {
      return FALSE;
    }

    unlink($filename);
    return TRUE;
  }

  /**
   * Determine if a file should be skipped from a preload script.
   *
   * @param \SplFileInfo $file
   *   FileInfo object for the file to be filtered.
   * @param string[] $exclude_list
   *   List of directories that should be excluded.
   *
   * @return bool
   *   True if the file should be skipped from preload script.
   */
  protected function shouldSkipFile(\SplFileInfo $file, array $exclude_list): bool {
    // Check exclude_list first.
    foreach ($exclude_list as $exclude_dir) {
      if (strpos($file->getRealPath(), $exclude_dir) === 0) {
        return TRUE;
      }
    }

    // Let's just always preload all Drupal-specific files.
    $allowed_extensions = ['module', 'theme', 'inc'];
    if (in_array($file->getExtension(), $allowed_extensions)) {
      return FALSE;
    }

    $name = $file->getFilename();
    // If the file name is all lowercase, there is a good chance it only has
    // functions and shouldn't be preloaded.
    // We even filter out files like bootstrap.php, settings.php, and composer's
    // autoload_static.php. This is what we want.
    if ($name == strtolower($name) && $file->getExtension() == 'php') {
      return TRUE;
    }

    return FALSE;
  }

}
