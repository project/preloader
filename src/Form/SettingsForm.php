<?php

namespace Drupal\preloader\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\preloader\PreloaderGenerator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure PHP Preloader settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  protected $generator;

  public function __construct(ConfigFactoryInterface $config_factory, PreloaderGenerator $preloader_generator)
  {
    $this->generator = $preloader_generator;
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('config.factory'),
      $container->get('preloader.generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'preloader_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['preloader.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['excluded_directories'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Excluded Directories'),
      '#description' => $this->t('List paths (case-sensitive) you want to exclude with each on its own line. The paths MUST be relative to your Drupal installation. e.g., modules/custom'),
      '#rows' => 4,
      '#resizable' => 'vertical',
      '#default_value' => $this->config('preloader.settings')->get('excluded_directories'),
    ];
    $form['target_filename'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Target filename'),
      '#description' => $this->t('Should be in a writable location by the webserver. Should end in .php.'),
      '#default_value' => $this->config('preloader.settings')->get('target_filename') ?: $this->generator->getDefaultTargetFilename(),
    ];

    $form = parent::buildForm($form, $form_state);

    $form['actions']['submit']['#value'] = $this->t('Save');
    $form['actions']['generate'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save and generate'),
      '#id' => 'generate',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $filename = $form_state->getValue('target_filename');
    if (!$this->generator->isWritable($filename)) {
      $form_state->setErrorByName('target_filename', $this->t('The specified file is not writable'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $target_filename = $form_state->getValue('target_filename');
    $this->config('preloader.settings')
      ->set('excluded_directories', $form_state->getValue('excluded_directories'))
      ->set('target_filename', $target_filename)
      ->save();
    parent::submitForm($form, $form_state);

    $trigger = $form_state->getTriggeringElement();
    if ($trigger['#id'] != 'generate') {
      return;
    }

    // @TODO: Refactor this into a service.
    $exclude_list = explode("\n", $form_state->getValue('excluded_directories'));
    $exclude_list = array_filter(array_map('trim', $exclude_list));

    $this->generator->writePreloaderScript($target_filename, $exclude_list);
    $this->messenger->addMessage($this->t('Generated a preload script at "%s". Set that absolute path to opcache.preload in your PHP settings.', ['%s' => $target_filename]));
  }

}
