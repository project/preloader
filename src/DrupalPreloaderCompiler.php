<?php

namespace Drupal\preloader;

use DarkGhostHunter\Preloader\PreloaderCompiler;
use Drupal\Core\Extension\ModuleExtensionList;

class DrupalPreloaderCompiler extends PreloaderCompiler {

  /**
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  public function __construct(ModuleExtensionList $extension_list) {
    $this->moduleExtensionList = $extension_list;
  }

  /**
   * {@inheritdoc}
   */
  public function compile(): string {
    $template = file_get_contents(__DIR__ . '/../assets/preload.php.stub');

    $psr4_loader = '';
    foreach ($this->moduleExtensionList->getList() as $name => $extension) {
      $path = DRUPAL_ROOT . '/' . $extension->getPath() . '/src';
      $psr4_loader .= "\$loader->addPsr4('Drupal\\{$name}\\\\', '{$path}');" . \PHP_EOL;
    }

    $replacing = array_merge($this->preloaderConfig, $this->opcacheConfig, [
      '@output' => $this->scriptRealPath(),
      '@generated_at' => date('Y-m-d H:i:s e'),
      '@autoload' => 'require_once \'' . realpath($this->autoloader) . '\';',
      '@list' => $this->parseList(),
      '@failure' => $this->ignoreNotFound ? 'continue;' : 'throw new \Exception("{$file} does not exist or is unreadable.");',
      '@psr4_loader' => $psr4_loader,
    ]);

    return str_replace(array_keys($replacing), $replacing, $template);
  }

}
